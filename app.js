// 1. Цикли існують для того, щоб перевиконувати код зазначену кількість разів
// 2. Використав цикл для цієї таски:)
// 3. Неявне перетворення відбувається автоматично. Явне відбувається через функції.

let lastNum = +prompt("enter a number")

function getNum() {
  for (let i = 1; i <= lastNum; i++) {
    if (i % 5 == 0) {
      console.log(i)
    }
  }
  if (lastNum < 5) {
    console.log("Sorry, no numbers")
  }
}
getNum()
